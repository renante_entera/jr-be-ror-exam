Rails.application.routes.draw do
  resources :posts, param: :slug do
    resources :comments, only: [:create, :destroy]
  end

  devise_for :users, controllers: {
    registrations: 'users/registrations'
  }
  devise_scope :user do
    get 'sign-up', to: 'devise/registrations#new', as: :sign_up
    get 'edit-profile', to: 'devise/registrations#edit', as: :edit_profile
    get 'sign-in', to: 'devise/sessions#new', as: :sign_in
  end

  root :to => 'pages#index'
  get 'my-news', to: "pages#my_news", :as => :user_root
  get 'change-password', to: 'passwords#edit', as: :change_password
  patch 'update-password', to: 'passwords#update', as: :update_password
end
