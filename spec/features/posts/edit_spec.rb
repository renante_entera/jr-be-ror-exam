require "rails_helper"

RSpec.feature "Posts#edit", type: :feature do
  include LoginSupport
  let(:user) { create(:user) }
  let(:post) { user.posts.create(attributes_for(:post)) }

  scenario "Current user edits own post" do
    sign_in_as user
    visit post_path(post)
    click_link "Edit Post"

    expect(page).to have_current_path edit_post_path(post)
    expect(page).to have_selector "h1", text: "Edit Post"
    expect(page).to have_field "post[title]"
    expect(page).to have_field "post[description]"
    expect(page).to have_field "post[keywords]"
    expect(page).to have_content post.title
    expect(page).to have_content post.description
    expect(page).to have_content post.keywords
    expect(page).to have_button "Save Post"
  end
end
