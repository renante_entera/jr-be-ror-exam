require "rails_helper"

RSpec.feature "Posts#new", type: :feature do
  include LoginSupport
  let(:user) { create(:user) }

  scenario "Current user visits create new post form" do
    sign_in_as user
    click_link "Add News"

    expect(page).to have_current_path new_post_path
    expect(page).to have_selector "h1", text: "New Post"
    expect(page).to have_field "post[title]"
    expect(page).to have_field "post[description]"
    expect(page).to have_field "post[keywords]"
    expect(page).to have_button "Create Post"
  end
end
