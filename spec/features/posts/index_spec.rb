require "rails_helper"

RSpec.feature "Posts#index", type: :feature do
  include LoginSupport
  let!(:user) { create(:user) }
  let!(:posts) { create_list(:post, 5, user: user) }

  scenario "Valid user redirects to MY NEWS" do
    sign_in_as user
    expect(page).to have_current_path user_root_path
    expect(page).to have_content "MY NEWS"

    page.all(".posts-link").each_with_index do |post, index|
      expect(post).to have_css(".posts-comments", text: posts.reverse[index].comments.count)
      expect(post).to have_css(".posts-text", text: posts.reverse[index].title)
    end
  end
end
