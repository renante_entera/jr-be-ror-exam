require "rails_helper"

RSpec.feature "Posts#show", type: :feature do
  include LoginSupport
  let!(:user) { create(:user) }
  let!(:posts) { create_list(:post, 5, user: user) }

  scenario "Current user shows own post" do
    sign_in_as user

    page.all(".posts-link").each_with_index do |link, index|
      link.click

      expect(page).to have_current_path post_path(posts.reverse[index])
      expect(page).to have_selector "h1", text: posts.reverse[index].title
      expect(page).to have_selector "div.post-text", text: posts.reverse[index].description
      expect(page).to have_link "Edit Post", href: edit_post_path(posts.reverse[index])
    end
  end
end
