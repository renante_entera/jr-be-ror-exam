require "rails_helper"

RSpec.feature "Posts#destroy", type: :feature do
  include LoginSupport
  let(:user) { create(:user) }
  let!(:post) { user.posts.create(attributes_for(:post)) }

  scenario "User deletes his own post" do
    sign_in_as user

    expect do
      visit post_path(user.posts.last)
      click_link "Edit Post"
      click_link "Delete"

      expect(page).to have_current_path user_root_path
    end.to change(user.posts, :count).by(-1)
  end
end
