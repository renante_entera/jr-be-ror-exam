require "rails_helper"

RSpec.feature "Posts#create", type: :feature do
  include LoginSupport
  let(:user) { create(:user) }
  let(:post) { build(:post) }

  scenario "Creates a new post with valid attributes" do
    sign_in_as user

    expect do
      valid_attributes_for_post

      expect(page).to have_current_path edit_post_path(user.posts.last)
      expect(page).to have_content "Post was successfully created."
      expect(page).to have_content post.title
    end.to change(user.posts, :count).by(1)
  end

  scenario "Creates a new post with invalid attributes" do
    sign_in_as user

    expect do
      invalid_attributes_for_post

      expect(page).to have_current_path posts_path
      expect(page).to have_content "Please review the problems below:"
    end.to change(user.posts, :count).by(0)
  end
end

def valid_attributes_for_post
  click_link "Add News"
  fill_in "Title", with: post.title
  fill_in "Content", with: post.description
  fill_in "Keywords", with: post.keywords
  click_button "Create Post"
end

def invalid_attributes_for_post
  click_link "Add News"
  fill_in "Title", with: post.title
  click_button "Create Post"
end
