require "rails_helper"

RSpec.feature "Comments#destroy", type: :feature do
  include LoginSupport
  let(:user) { create(:user) }
  let!(:post) { user.posts.create(attributes_for(:post)) }
  let!(:comment) { post.comments.create(attributes_for(:comment, user: user)) }

  scenario "User deletes own comment to a post" do
    sign_in_as user

    expect do
      visit post_path(user.posts.last)
      page.all(".comment-action a")[0].click

      expect(page).to have_current_path post_path(post)
    end.to change(user.posts.last.comments, :count).by(-1)
  end
end
