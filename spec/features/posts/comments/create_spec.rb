require "rails_helper"

RSpec.feature "Comments#create", type: :feature do
  include LoginSupport
  let(:user) { create(:user) }
  let(:post) { user.posts.create(attributes_for(:post)) }
  let(:comment) { build(:comment) }

  scenario "Creates a new comment to a post" do
    sign_in_as user

    expect do
      visit post_path(post)
      fill_in "comment[body]", with: comment.body
      click_button "SUBMIT"

      expect(page).to have_current_path post_path(post)
    end.to change(post.comments, :count).by(1)
  end
end
