require "rails_helper"

RSpec.feature "Posts#update", type: :feature do
  include LoginSupport
  let(:user) { create(:user) }
  let(:post) { user.posts.create(attributes_for(:post)) }
  let(:new_post) do
    user.posts.build(attributes_for(:post, title:    "New title",
                                           keywords: "new keyword 1, new keyword 2"))
  end

  scenario "Current user updates his own post" do
    sign_in_as user

    expect do
      visit post_path(post)
      click_link "Edit Post"

      fill_in "post[title]", with: new_post.title
      fill_in "post[keywords]", with: new_post.keywords
      click_button "Save Post"

      expect(page).to have_current_path edit_post_path(post.reload)
      expect(page).to have_selector "h1", text: "Edit Post"
      expect(post.reload.title).to eq(new_post.title)
      expect(post.reload.keywords).to eq(new_post.keywords)
    end.to change(user.posts, :count).by(1)
  end
end
