# require "rails_helper"

# RSpec.feature "Custom Passwords#update", type: :feature do
#   include LoginSupport
#   let(:user) { create(:user) }
#   let(:current_password) { user.password }
#   let(:new_password) { "newpassword" }

#   context "When request is valid" do
#     scenario "User redirected to NEWS with successful notice" do
#       valid_parameters

#       expect(page).to have_current_path root_path
#       expect(page).to have_content("Your password has been changed successfully.")
#     end
#   end

#   context "When request is invalid" do
#     scenario "User does not provide any parameters" do
#       no_parameters

#       expect(page).to have_current_path update_password_path
#       expect(page).to have_content("New Password can't be blank")
#       expect(page).to have_content("Password Confirmation can't be blank")
#       expect(page).to have_content("Current Password can't be blank")
#     end
#   end
# end

# def valid_parameters
#   sign_in_as user
#   visit change_password_path
#   fill_in "user_password", with: new_password
#   fill_in "user_password_confirmation", with: new_password
#   fill_in "user_current_password", with: current_password
#   click_button "Change password"
# end

# def no_parameters
#   sign_in_as user
#   visit change_password_path
#   fill_in "user_password", with: nil
#   fill_in "user_password_confirmation", with: nil
#   fill_in "user_current_password", with: nil
#   click_button "Change password"
# end
