require "rails_helper"

RSpec.feature "Sessions#destroy", type: :feature do
  include LoginSupport
  let(:user) { create(:user) }

  scenario "Current logged-in user signs out" do
    sign_in_as user
    click_link "Sign Out"

    expect(page).to have_current_path root_path
    expect(page).to have_content "Signed out successfully."
  end
end
