require "rails_helper"

RSpec.feature "Sessions#new", type: :feature do
  scenario "User visits Sign In page" do
    visit root_path
    click_link "Sign In"

    expect(page).to have_current_path sign_in_path
    expect(page).to have_css("h2.login-register-header-title")
    expect(page).to have_content "LOGIN"
  end
end
