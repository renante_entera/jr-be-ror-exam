require "rails_helper"

RSpec.feature "Sessions#create", type: :feature do
  include LoginSupport
  let(:user) { create(:user) }

  scenario "Signing in with correct credentials" do
    sign_in_as user

    expect(page).to have_current_path user_root_path
    expect(page).to have_content "Signed in successfully."
  end

  scenario "Signing in with invalid credentials" do
    user.password = "wrongpassword"
    sign_in_as user

    expect(page).to have_current_path new_user_session_path
    expect(page).to have_content "Invalid Username or password."
  end
end
