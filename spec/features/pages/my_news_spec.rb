require "rails_helper"

RSpec.feature "Pages#my_news", type: :feature do
  include LoginSupport
  let!(:user) { create(:user) }
  let!(:posts) { create_list(:post, 5, user: user) }

  scenario "Displays user's posts in MY NEWS" do
    sign_in_as user
    expect(page).to have_current_path user_root_path

    page.all(".posts-item").each_with_index do |item, index|
      expect(item).to have_link(nil, href: post_path(posts.reverse[index]))
      expect(item).to have_selector(".posts-comments", text: posts.reverse[index].comments.count)
      expect(item).to have_selector(".posts-text", text: posts.reverse[index].title)
    end
  end
end
