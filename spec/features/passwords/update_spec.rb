require "rails_helper"

RSpec.feature "Passwords#update", type: :feature do
  let(:user) { create(:user) }
  let(:new_password) { "newpassword" }

  scenario "User changes his password" do
    visit edit_user_password_path(reset_password_token: user.send_reset_password_instructions)
    fill_in "user_password", with: new_password
    fill_in "user_password_confirmation", with: new_password
    click_button "Change my password"

    expect(page).to have_current_path user_root_path
    expect(page).to have_content("Your password has been changed successfully. " \
      "You are now signed in.")
  end
end
