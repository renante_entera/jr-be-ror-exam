require "rails_helper"

RSpec.feature "Passwords#create", type: :feature do
  let(:user) { create(:user) }

  scenario "User fills in forgot your password form" do
    visit new_user_password_path
    fill_in "user[email]", with: user.email
    click_on "Send me reset password instructions"

    expect(page).to have_current_path new_user_session_path
    expect(page).to have_content("You will receive an email with instructions" \
      " on how to reset your password in a few minutes.")
  end
end
