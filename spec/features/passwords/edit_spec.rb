require "rails_helper"

RSpec.feature "Passwords#edit", type: :feature do
  let(:user) { build(:user) }

  scenario "User visits change your password page" do
    visit edit_user_password_path(reset_password_token: user.send_reset_password_instructions)

    expect(page).to have_content("Change your password")
    expect(page).to have_field("user_password")
    expect(page).to have_field("user_password_confirmation")
    expect(page).to have_button("Change my password")
  end
end
