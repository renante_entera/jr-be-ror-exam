require "rails_helper"

RSpec.feature "Passwords#new", type: :feature do
  scenario "User visits forgot your password page" do
    visit root_path
    click_link "Sign In"
    click_link "CLICK HERE"

    expect(page).to have_current_path new_user_password_path
    expect(page).to have_content("Forgot your password?")
    expect(page).to have_field("email")
    expect(page).to have_button("Send me reset password instructions")
  end
end
