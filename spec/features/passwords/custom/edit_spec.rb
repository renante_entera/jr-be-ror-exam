require "rails_helper"

RSpec.feature "Custom Passwords#edit", type: :feature do
  include LoginSupport
  let(:user) { create(:user) }

  scenario "User visits change password page" do
    sign_in_as user
    click_link "Change Password"

    expect(page).to have_content("Change password")
    expect(page).to have_field("user_password")
    expect(page).to have_field("user_password_confirmation")
    expect(page).to have_field("user_current_password")
    expect(page).to have_button("Change password")
  end
end
