require "rails_helper"

RSpec.feature "Users#new", type: :feature do
  scenario "Guest user visits registration page" do
    visit root_path
    click_link "Sign In"
    click_link "REGISTER HERE"

    expect(page).to have_current_path new_user_registration_path
    expect(page).to have_text("Register")
    expect(page).to have_field("username")
    expect(page).to have_field("email")
    expect(page).to have_field("user_password")
    expect(page).to have_field("user_password_confirmation")
    expect(page).to have_button("SUBMIT")
  end
end
