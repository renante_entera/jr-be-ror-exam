require "rails_helper"

def edit_profile
  sign_in_as user
  click_link "Edit Profile"
end

RSpec.feature "Users#update", type: :feature do
  include LoginSupport
  let(:user) { create(:user) }

  scenario "Update user profile with valid attributes" do
    edit_profile

    fill_in "user[username]", with: user.username
    fill_in "user[email]", with: user.email
    click_button "UPDATE"

    expect(page).to have_current_path user_root_path
    expect(page).to have_content("MY NEWS")
  end

  scenario "Update user profile with no email" do
    edit_profile

    fill_in "user[username]", with: user.username
    fill_in "user[email]", with: nil
    click_button "UPDATE"

    expect(page).to have_current_path user_registration_path
    expect(page).to have_content("Email can't be blank")
  end
end
