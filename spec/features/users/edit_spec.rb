require "rails_helper"

RSpec.feature "Users#edit", type: :feature do
  include LoginSupport
  let(:user) { create(:user) }

  scenario "User visits edit profile page" do
    sign_in_as user
    click_link "Edit Profile"

    expect(page).to have_current_path edit_profile_path
    expect(page).to have_field("username")
    expect(page).to have_field("email")
    expect(page).to have_button("UPDATE")
  end
end
