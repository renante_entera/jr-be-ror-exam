require "rails_helper"

RSpec.describe PostsController, type: :request do
  before do
    @user = login_user
    @headers = { Accept: "application/json", 'Content-Type': "application/json" }
  end

  describe "PATCH /update" do
    context "When request is valid" do
      before do
        @post = @user.posts.create(attributes_for(:post))
        @post_params = { title: "new title", content: "new content" }
        patch post_path(@post.slug), params: @post_params.to_json, headers: @headers
      end

      it "redirects to Edit Post on update" do
        expect(response).to redirect_to(edit_post_path(slug: "new-title"))
      end

      it "flashes a success message" do
        expect(response.request.flash[:notice]).to_not be_nil
      end

      it "flashes a notice message updated" do
        expect(response.request.flash[:notice]).to eq("Post was successfully updated.")
      end
    end
  end
end
