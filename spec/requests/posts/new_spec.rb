require "rails_helper"

RSpec.describe PostsController, type: :request do
  describe "GET /new" do
    before do
      @user = login_user

      get new_post_path
    end

    it "returns a successful response" do
      expect(response).to be_successful
    end

    it "returns http success" do
      expect(response).to have_http_status(:success)
    end

    it "returns response body with header New Post" do
      expect(response.body).to include("<h1>New Post</h1>")
    end
  end
end
