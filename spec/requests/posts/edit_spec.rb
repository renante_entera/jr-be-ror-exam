require "rails_helper"

RSpec.describe PostsController, type: :request do
  describe "GET /edit" do
    before do
      @user = login_user
      @post = @user.posts.create(attributes_for(:post))

      get edit_post_path(@post.slug)
    end

    it "returns a successful response" do
      expect(response).to be_successful
    end

    it "returns http success" do
      expect(response).to have_http_status(:success)
    end

    it "returns response body with header Edit Post" do
      expect(response.body).to include("<h1>Edit Post</h1>")
    end
  end
end
