require "rails_helper"

RSpec.describe PostsController, type: :request do
  describe "GET /index" do
    before do
      @user = login_user
      @post = @user.posts.create(attributes_for(:post))

      get posts_path
    end

    it "redirects to MY NEWS" do
      expect(response).to redirect_to(user_root_path)
    end
  end
end
