require "rails_helper"

RSpec.describe PostsController, type: :request do
  describe "GET /show" do
    before do
      @user = login_user
      @post = @user.posts.create(attributes_for(:post))

      get post_path(@post.slug)
    end

    it "returns a successful response" do
      expect(response).to be_successful
    end

    it "returns http success" do
      expect(response).to have_http_status(:success)
    end

    it "returns response body with post title in post body" do
      expect(response.body).to include("<h1 class=\"post-title\">#{@post.title}</h1>")
    end

    it "returns response body with comment form for logged in user" do
      expect(response.body).to include('<div class="comment">')
    end
  end
end
