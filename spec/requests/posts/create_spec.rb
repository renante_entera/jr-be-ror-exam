require "rails_helper"

RSpec.describe PostsController, type: :request do
  describe "POST /create" do
    before do
      @user = login_user
      @post = @user.posts.build(attributes_for(:post, slug: nil))
      @headers = {
        Accept: "application/json", 'Content-Type': "application/json"
      }
      post posts_path, params: @post.to_json, headers: @headers
    end

    it "redirects to Edit Post on save" do
      @post_slug = [@post.title.to_s.parameterize].join("-")
      expect(response).to redirect_to(edit_post_url(slug: @post_slug))
    end

    it "flashes a success message" do
      expect(response.request.flash[:notice]).to_not be_nil
    end

    it "flashes a notice message created" do
      expect(response.request.flash[:notice]).to eq("Post was successfully created.")
    end
  end
end
