require "rails_helper"

RSpec.describe PagesController, type: :request do
  describe "GET /index" do
    before do
      create(:user) do |user|
        @post = user.posts.create(attributes_for(:post))
      end
      get root_path
    end

    it "returns http success" do
      expect(response).to have_http_status(:success)
    end

    it "returns a successful response" do
      expect(response).to be_successful
    end

    it "returns response body with header NEWS" do
      expect(response.body).to include('<h1 class="posts-title">NEWS</h1>')
    end

    it "returns response body with post title in the list" do
      expect(response.body).to include("<p class=\"posts-text\">#{@post.title}</p>")
    end

    it "returns response body with zero post comment" do
      expect(response.body).to include("<div class=\"posts-comments\">0</div>")
    end
  end
end
