require "rails_helper"

RSpec.describe PagesController, type: :request do
  describe "GET /my_news" do
    before do
      @user = login_user
      @post = @user.posts.create(attributes_for(:post))

      get user_root_path
    end

    it "returns http success" do
      expect(response).to have_http_status(:success)
    end

    it "returns a successful response" do
      expect(response).to be_successful
    end

    it "returns response body with header MY NEWS" do
      expect(response.body).to include('<h1 class="posts-title">MY NEWS</h1>')
    end

    it "returns response body with post title in MY NEWS list" do
      expect(response.body).to include("<p class=\"posts-text\">#{@post.title}</p>")
    end

    it "returns response body with zero post comment" do
      expect(response.body).to include("<div class=\"posts-comments\">0</div>")
    end
  end
end
