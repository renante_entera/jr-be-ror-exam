FactoryBot.define do
  factory :post do
    sequence(:title) { |n| "Post title#{n}" }
    sequence(:slug) { |n| "post-title#{n}" }
    description { "Post content" }
    user { nil }
    published { true }
    keywords { "keyword 1, keyword 2" }
  end
end
