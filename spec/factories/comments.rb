FactoryBot.define do
  factory :comment do
    body { "Comment body" }
    commentable { nil }
    user { nil }
  end
end
