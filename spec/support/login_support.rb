module LoginSupport
  def sign_in_as(user)
    visit root_path
    click_link "Sign In"
    fill_in "Username", with: user.username
    fill_in "Password", with: user.password
    click_button "LOGIN"
  end
end
