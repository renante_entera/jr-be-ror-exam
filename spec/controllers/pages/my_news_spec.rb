require "rails_helper"

RSpec.describe PagesController do
  describe "GET #my_news" do
    before do
      @user = create(:user)
      sign_in @user
      @posts = create_list(:post, 5, user: @user)
      get :my_news
    end

    it "renders the :my_news view" do
      expect(subject).to render_template(:my_news)
    end

    it "populates an array of posts" do
      expect(@posts).to eq(@user.posts)
    end
  end
end
