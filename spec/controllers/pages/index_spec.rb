require "rails_helper"

RSpec.describe PagesController do
  describe "GET #index" do
    before do
      @user = create(:user)
      @posts = create_list(:post, 5, user: @user)
      get :index
    end

    it "renders the :index view" do
      expect(subject).to render_template(:index)
    end

    it "populates an array of posts" do
      expect(@posts).to eq(@user.posts)
    end
  end
end
