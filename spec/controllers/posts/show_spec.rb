require "rails_helper"

RSpec.describe PostsController do
  describe "GET #show" do
    before do
      @user = create(:user)
      sign_in @user
      @post = @user.posts.create(attributes_for(:post))
      get :show, params: { slug: @post.slug }
    end

    it "renders the :show view" do
      expect(response).to render_template(:show)
    end
  end
end
