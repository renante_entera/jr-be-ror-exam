require "rails_helper"

RSpec.describe PostsController do
  describe "PATCH #update" do
    before do
      @user = create(:user)
      @post = @user.posts.create(attributes_for(:post))
      @post_params = attributes_for(:post, user: @user, title: "New Title")
      sign_in @user
    end

    it "updates an existing post" do
      patch :update, params: { slug: @post.slug, post: @post_params }
      expect(@post.reload).to eq @user.posts.last
    end

    it "redirects to Edit Post on update" do
      post :create, params: { post: @post_params }
      expect(response).to redirect_to edit_post_path @user.posts.last.slug
      expect(response).to redirect_to edit_post_url(slug: @user.posts.last.slug)
    end
  end
end
