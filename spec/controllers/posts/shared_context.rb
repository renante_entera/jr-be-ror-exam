RSpec.shared_context "create valid post params" do
  context "with valid post params" do
    subject do
      @post_params = attributes_for(:post, user: @user)
      post :create, params: { post: @post_params }
    end

    it "creates a new post successful" do
      expect do
        subject
      end.to change(@user.posts, :count).by(1)
    end

    it "redirects to Edit Post on save" do
      expect(subject).to redirect_to edit_post_path @user.posts.last
      expect(subject).to redirect_to edit_post_url(slug: @user.posts.last)
    end
  end
end

RSpec.shared_context "create invalid post params" do
  context "with invalid post params" do
    subject do
      @post_params = attributes_for(:post, user: @user, title: nil)
      post :create, params: { post: @post_params }
    end

    it "creates a new post failed" do
      expect do
        subject
      end.to change(@user.posts, :count).by(0)
    end
  end
end
