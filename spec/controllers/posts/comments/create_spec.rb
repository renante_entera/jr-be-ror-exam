require "rails_helper"

RSpec.describe CommentsController do
  describe "POST #create" do
    before do
      @user = create(:user)
      @post = @user.posts.create(attributes_for(:post))
      @comment_params = attributes_for(:comment, commentable: @post, user: @user)
      sign_in @user
    end

    it "creates a new comment" do
      expect do
        post :create, params: { post_slug: @post.slug, comment: @comment_params }
      end.to change(@post.comments, :count).by(1)
    end

    it "redirects to Commentable on save" do
      post :create, params: { post_slug: @post.slug, comment: @comment_params }
      expect(response).to redirect_to post_path @post
    end
  end
end
