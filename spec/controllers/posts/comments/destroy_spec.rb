require "rails_helper"

RSpec.describe CommentsController do
  describe "DELETE #destroy" do
    before do
      @user = create(:user)
      @post = @user.posts.create(attributes_for(:post))
      @comment = @post.comments.create(attributes_for(:comment, user: @user))
      sign_in @user
    end

    it "deletes an existing comment" do
      expect do
        delete :destroy, params: { post_slug: @post.slug, id: @comment.id }
      end.to change(@post.comments, :count).by(-1)
    end

    it "redirects to Commentable on delete" do
      delete :destroy, params: { post_slug: @post.slug, id: @comment.id }
      expect(response).to redirect_to post_path @post
    end
  end
end
