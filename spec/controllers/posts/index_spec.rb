require "rails_helper"

RSpec.describe PostsController do
  describe "GET #index" do
    before do
      sign_in create(:user)
      get :index
    end

    it "redirects to MY NEWS" do
      expect(response).to redirect_to user_root_path
    end
  end
end
