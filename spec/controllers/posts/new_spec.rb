require "rails_helper"

RSpec.describe PostsController do
  describe "GET #new" do
    before do
      sign_in create(:user)
      get :new
    end

    it "renders the :new view" do
      expect(response).to render_template(:new)
    end
  end
end
