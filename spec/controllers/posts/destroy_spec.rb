require "rails_helper"

RSpec.describe PostsController do
  describe "DELETE #destroy" do
    before do
      @user = create(:user)
      @post = @user.posts.create(attributes_for(:post))
      sign_in @user
    end

    it "deletes an existing post" do
      expect do
        delete :destroy, params: { slug: @post.slug }
      end.to change(@user.posts, :count).by(-1)
    end

    it "redirects to MY NEWS on delete" do
      delete :destroy, params: { slug: @post.slug }
      expect(response).to redirect_to posts_path
    end
  end
end
