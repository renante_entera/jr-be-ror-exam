require "rails_helper"

RSpec.describe PostsController do
  describe "GET #edit" do
    before do
      @user = create(:user)
      @post = @user.posts.create(attributes_for(:post))
      sign_in @user
      get :edit, params: { slug: @post.slug }
    end

    it "renders the :edit view" do
      expect(response).to render_template(:edit)
    end
  end
end
