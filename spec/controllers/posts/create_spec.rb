require "rails_helper"
require_relative "shared_context"

RSpec.describe PostsController do
  describe "POST #create" do
    before do
      @user = create(:user)
      sign_in @user
    end

    include_context "create valid post params"

    include_context "create invalid post params"
  end
end
