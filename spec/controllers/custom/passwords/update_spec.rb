require "rails_helper"

RSpec.describe PasswordsController do
  describe "PATCH #update" do
    before do
      load_user
    end

    context "When request is valid" do
      before do
        @current_password = @user.password
        patch_update_with_params
      end

      it "user sets a new password and redirects to NEWS" do
        @user.reload
        expect(@user.valid_password?(@new_password)).to be(true)
        expect(response).to redirect_to root_path
      end
    end

    context "When request is invalid" do
      before do
        @current_password = nil
        patch_update_with_params
      end

      it "user does not provide valid attributes" do
        expect(@user.valid_password?(@new_password)).not_to be(true)
      end
    end
  end
end

def load_user
  @user = create(:user)
  sign_in @user
  @new_password = "newpassword"
end

def patch_update_with_params
  patch :update, params: { "user" => { "current_password"      => @current_password,
                                       "password"              => @new_password,
                                       "password_confirmation" => @new_password } }
end
