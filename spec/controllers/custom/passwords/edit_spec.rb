require "rails_helper"

RSpec.describe PasswordsController do
  describe "GET #edit" do
    before do
      @user = create(:user)
      sign_in @user
      get :edit
    end

    it "returns a 200 response" do
      expect(response).to have_http_status :ok
    end

    it "renders the :edit view" do
      expect(response).to render_template(:edit)
    end
  end
end
