require "rails_helper"

RSpec.describe Users::RegistrationsController do
  describe "GET #edit" do
    before do
      sign_in create(:user)
      @request.env["devise.mapping"] = Devise.mappings[:user]
      get :edit
    end

    it "returns a 200 response" do
      expect(response).to have_http_status "200"
    end

    it "renders the :edit view" do
      expect(response).to render_template(:edit)
    end
  end
end
