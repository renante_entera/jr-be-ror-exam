require "rails_helper"

RSpec.describe Users::RegistrationsController do
  describe "PATCH #update" do
    before do
      @user = create(:user)
      sign_in @user
      @user_params = attributes_for(:user, username: "newusername")
      @request.env["devise.mapping"] = Devise.mappings[:user]
    end

    it "updates an existing user" do
      patch :update, params: { id: @user.id, user: @user_params }
      expect(@user.reload[:username]).to eq @user_params[:username]
      expect(@user.reload[:email]).to eq @user_params[:email]
    end

    it "redirects to My News after update" do
      patch :update, params: { id: @user.id, user: @user_params }
      expect(response).to redirect_to user_root_path
    end
  end
end
