require "rails_helper"

RSpec.describe Users::RegistrationsController do
  describe "POST #create" do
    before do
      @user_params = attributes_for(:user)
      @request.env["devise.mapping"] = Devise.mappings[:user]
    end

    it "creates a new user" do
      expect do
        post :create, params: { user: @user_params }
      end.to change(User, :count).by(1)
    end

    it "redirects to NEWS page after register" do
      post :create, params: { user: @user_params }
      expect(response).to redirect_to root_path
    end
  end
end
