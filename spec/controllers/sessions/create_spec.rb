require "rails_helper"

RSpec.describe Devise::SessionsController do
  describe "POST #create" do
    before do
      @request.env["devise.mapping"] = Devise.mappings[:user]
      @user = build(:user)
      post :create, params: { user: @user }
    end

    it "returns a 200 response" do
      expect(response).to have_http_status :ok
    end
  end
end
