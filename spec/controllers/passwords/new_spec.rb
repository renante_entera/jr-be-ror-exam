require "rails_helper"

RSpec.describe Devise::PasswordsController do
  describe "GET #new" do
    before do
      @request.env["devise.mapping"] = Devise.mappings[:user]
      get :new
    end

    it "renders the :new view" do
      expect(response).to render_template(:new)
    end
  end
end
