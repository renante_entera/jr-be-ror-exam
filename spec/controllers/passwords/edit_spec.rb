require "rails_helper"

RSpec.describe Devise::PasswordsController do
  describe "GET #edit" do
    before do
      @request.env["devise.mapping"] = Devise.mappings[:user]
      @user = create(:user)
      @raw  = @user.send_reset_password_instructions
      get :edit, params: { reset_password_token: @raw }
    end

    it "returns a 200 response" do
      expect(response).to have_http_status :ok
    end

    it "renders the :edit view" do
      expect(response).to render_template(:edit)
    end
  end
end
