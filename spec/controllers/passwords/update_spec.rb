require "rails_helper"
require "bcrypt"

def put_update_with_params
  put :update, params: { "user" => { "reset_password_token"  => @raw,
                                     "password"              => @new_password,
                                     "password_confirmation" => @new_password } }
end

RSpec.describe Devise::PasswordsController do
  describe "PUT #update" do
    before do
      @request.env["devise.mapping"] = Devise.mappings[:user]
      @user = create(:user)
      @raw  = @user.send_reset_password_instructions
      @old_password = @user.encrypted_password
      @new_password = "newpassword"
      put_update_with_params
    end

    it "resets user's password" do
      @user.reload
      expect(@user.encrypted_password).not_to eq(@old_password)
    end

    it "redirects to MY NEWS" do
      expect(response).to redirect_to user_root_path
    end
  end
end
