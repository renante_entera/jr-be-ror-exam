require "rails_helper"

RSpec.describe Devise::PasswordsController do
  describe "POST #create" do
    before do
      @request.env["devise.mapping"] = Devise.mappings[:user]
      @user = build(:user)
      post :create
    end

    it "returns a 200 response" do
      expect(response).to have_http_status :ok
    end
  end
end
