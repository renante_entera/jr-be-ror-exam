class AddPhotoUploadToUsers < ActiveRecord::Migration[6.1]
  def change
    add_column :users, :photo_upload, :string, after: :email
  end
end
