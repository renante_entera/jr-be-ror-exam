class AddKeywordsToPosts < ActiveRecord::Migration[6.1]
  def change
    add_column :posts, :keywords, :string
  end
end
