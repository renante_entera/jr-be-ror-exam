class AddPhotoUploadToPosts < ActiveRecord::Migration[6.1]
  def change
    add_column :posts, :photo_upload, :string, after: :description
  end
end
