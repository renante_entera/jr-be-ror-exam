
# JR ROR CRUDBLOG SKILL TEST
[Clone JR ror Exam](https://bitbucket.org/choyno/jr-be-ror-exam/src/master/)
```
 - Read the README.md for the installation and Starting the Server
 - Clone/Fork  this Repoand push it to your own Repo and add me as your reviewer or collaborator
	 Email : "ariel.lopezf@gmail.com" / Username: "choyno"
 - Maximum 4 Days of Skill Test
```

### Functionalities

### RULES
 Do not add/user any third party assets ex. ( boostrap, materialui)

### Home Page
```
  -	When clicking the menu Sign In should Redirect to Login user
  -	When clicking the menu Sign In should Redirect to Register user
  - Display Publish News only
  -	Make the Slider Workable ( Swipe Left and Right when click the arrow button )
  -	Display the 3 Latest News in the Slider
  -	News Section ( Display 8 News / Sort by Newly Created news )
  -	Load Mode ( increment to 8 )
  -	Make the Scroll Up workable
  - News List shoud display the News banner image / title / total comments
  - when clicking a single post should redirect to the details ( using routes param slug )
```

### Login page
```
	- Validated the presence of username and password
	- login via `Username`
	- Can Login user and Redirect to My News Page( user dasboard )
	- When clicking the REGISTER HERE should redirect to Sign up page
	-	Make the Scroll Up workable
```

### Registration page
```
	- validatate the presence of username, email, password
	- can Preview selected Profile Picture
	- can upload Profile Picture
	- Make the Registration Workable and Redirect to My News Page( user dasboard )
	- When clicking the LOGIN HERE should redirect to Sign in page
	- Make the Scroll Up workable
```
### My News
```
	- Display all the Created News by the Current user
	- Display Publish and UnPublish News
	- News Display 20 per page
	- Load Mode ( increment to 20 )
	- Can Create new News
	- When clickng the single News it will redirect to Edit news
	- Make the Scroll Up workable
```

### Create  News
```
	- Display Create News Form
	- Validate Presence of Title
	- Validate Presence of Content
	- Can Preview selected Banner Image
  - Set default sample banner image if not blank
	- Can upload Banner Image ( do not save base64 type upload )
	- After creating the new it should Redirect to Edit Page
	- Make the Scroll Up workable
```

### Edit  News
```
	- Display Create News Form
	- Validate Presence of Title
	- Validate Presence of Content
	- Can Preview selected Banner Image
	- Can upload Banner Image ( do not save base64 )
	- Can Publish and Unpublish News
	- After the updating Redirect to Edit Page
	- Can delete post apply soft delete
```

### View  News
```
	- Display the News Detail ( using routes param slug )
	- Can Edit Post
	- Can Edit the Current News if the news current user is the owner
	- Can Comment if the User logged in
	- New comment should display first in the list
	- Can delete Comment if the current user is the owner
```

### Edit Profile
```
	- Can Update all the information
	- Should validated presence username and email
	- Can upload Profile Image ( do not save base64 type )
	- When changing the password Current password should inputed
	- ignore change password if the current password is blank
	- should validated the New Password and Confirm Password
```

### Header Nav
```
	- Clickable Logo should redirect to Home Page / Root Page
	- if not sign in display ( Sign in ) menu
	- if Signed in display ( My News / Edit Profile / Sign out )
	- Click My News ( redirect to My News page )
	- Click Edit Profile ( redirect to Edit Profile page)
	- Click Sign out ( clear user and redirect to Homepage )
```
