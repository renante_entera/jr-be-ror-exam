document.addEventListener("click", function(event) {
  // for openFileDialog
  if (event.target.id == 'upload-image') {
    openFileDialog();
  }
});

document.addEventListener("change", function(event) {
  // for previewImage
  if (event.target.id == 'photo-upload') {
    previewImage(event);
  }
});

var openFileDialog = () => {
  var input = document.getElementById('photo-upload');
  input.click();
}

function previewImage(event) {
  var reader = new FileReader();
  reader.onload = function() {
    var output = document.getElementById('preview-image');
    output.style.backgroundImage = 'url(' + reader.result + ')';
  }
  reader.readAsDataURL(event.target.files[0]);
}