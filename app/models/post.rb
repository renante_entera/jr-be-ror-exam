class Post < ApplicationRecord
  after_validation :set_slug, only: %i[create update]
  after_validation :set_photo_upload, only: %i[create update]
  acts_as_paranoid
  belongs_to :user
  has_many :comments, as: :commentable, dependent: :destroy
  scope :published, -> { where(published: true) }

  validates :title, presence: true, length: { minimum: 6, maximum: 100 }
  validates :description, presence: true, length: { minimum: 10, maximum: 300 }

  def to_param
    slug
  end

  private

  def set_slug
    self.slug = [title.to_s.parameterize].join("-")
  end

  def set_photo_upload
    self.photo_upload = "h60c1tudxdnldlp6hqt4.png" if photo_upload.nil?
  end
end
