class PagesController < ApplicationController
  before_action :authenticate_user!, only: %i[my_news]

  def index
    @latest = Post.published.last(3).reverse
    @posts = Post.published.reverse
  end

  def my_news
    @posts = current_user.posts.reverse
  end
end
