class PostsController < ApplicationController
  before_action :set_post, only: %i[show edit update destroy]
  before_action :authenticate_user!, except: %i[show]
  before_action :set_commentable, only: %i[show]
  before_action :require_same_user, only: %i[edit update destroy]
  before_action :cloudinary_upload, only: %i[create update]

  # GET /posts or /posts.json
  def index
    redirect_to user_root_path
  end

  # GET /posts/1 or /posts/1.json
  def show; end

  # GET /posts/new
  def new
    @post = Post.new
  end

  # GET /posts/1/edit
  def edit; end

  # POST /posts or /posts.json
  def create
    @post = Post.new(post_params)
    @post.user = current_user
    @post.photo_upload = @uploaded["url"] unless @uploaded.nil?

    if @post.save
      flash[:notice] = "Post was successfully created."
      redirect_to edit_post_path(@post)
    else
      render "new"
    end
  end

  # PATCH/PUT /posts/1 or /posts/1.json
  def update
    new_params = post_params
    new_params[:photo_upload] = @uploaded["url"] unless @uploaded.nil?

    if @post.update(new_params)
      flash[:notice] = "Post was successfully updated."
      redirect_to edit_post_path(@post)
    else
      render "edit"
    end
  end

  # DELETE /posts/1 or /posts/1.json
  def destroy
    @post.destroy
    respond_to do |format|
      format.html { redirect_to posts_url, notice: "Post was successfully deleted." }
      format.json { head :no_content }
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_post
    @post = Post.find_by(slug: params[:slug])
  end

  # Only allow a list of trusted parameters through.
  def post_params
    params.require(:post).permit(:title, :description, :published, :photo_upload, :keywords)
  end

  # For polymorphic comments
  def set_commentable
    @commentable = Post.find_by(slug: params[:slug])
  end

  # Edit/delete post restriction
  def require_same_user
    @post = current_user.posts.find_by(slug: params[:slug])
  end

  def cloudinary_upload
    return unless post_params[:photo_upload]

    @uploaded = Cloudinary::Uploader.upload(post_params[:photo_upload])
  end
end
