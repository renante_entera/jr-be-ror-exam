class PasswordsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_user

  def edit; end

  def update
    @user = User.new(user_params)
    @user.validate

    @errors = @user.errors.messages
    if @errors.empty?
      current_user.update_with_password(user_params)
      # Sign in the user by passing validation in case their password changed
      bypass_sign_in(current_user)
      flash[:notice] = "Your password has been changed successfully."
      redirect_to root_path
    else
      render "edit"
    end
  end

  private

  def user_params
    params.require(:user).permit(:password, :password_confirmation, :current_password)
          .merge(id: current_user.id)
  end

  def set_user
    @user = current_user
  end
end
