# README

To change Ruby version https://rvm.io
```
RUBY 2.7.0
```

Using Rails Version
```
RAILS 6.1.3
```

To Change Node version https://github.com/hokaccha/nodebrew
```
NODE v14.15.3
```

Start Server
```
  rails s
```

MANUAL: Running Rubocop Server https://github.com/rubocop/rubocop
```
  bundle exec rubocop
```

MANUAL: Auto Correct Rubo Cup
```
 bundle exec rubocop --safe-auto-correct
```

MANUAL: Running Rails Best Practice https://github.com/flyerhzm/rails_best_practices
```
bundle exec rails_best_practices .
```

( USE THIS )AUTO RUN BOTH  RUBOCOP / RAILS BEST PRACTICE USING GUARD
```
 bundle exec guard
```
---

App Required Gems
```
  - Devise
  - Simple Form
  - Cloudinary ( third part image storage )
  - Act as Paranoid ( for soft deletion )
```

App Auto Checker
```
  - Rubocop ( development linter ) - LINK
  - Rails Best Practice ( community standard practices ) - LINK
```

Model
```
  - Comment Model should be a Reusable Polymorphic Association  Model
  - Devise using username as login
  - Image upload should not use base64 data
```
